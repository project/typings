
import * as Fs from 'fs';
import * as Glob from 'glob';

(function () : void {

  let typings: any = JSON.parse(Fs.readFileSync('typings.json', {'encoding': 'utf8'}));
  if (!typings.hasOwnProperty('files')) {
    process.stderr.write('typings.json has no "files"\n');
    process.exit(1);
  }

  if (!Array.isArray(typings.files)) {
    process.stderr.write('Type of typings.json/files is not array.\n');
    process.exit(2);
  }

  let listedFiles: string[] = typings.files.sort();
  let actualFiles = Glob.sync('source/*.d.ts').sort();

  if (listedFiles.length !== actualFiles.length) {
    process.stderr.write('Number of files in the typings.json/files and source/*.d.ts files are different\n');
    process.exit(3);
  }

  for (let f: number = 0; f < listedFiles.length; f++) {
    if (listedFiles[f] !== actualFiles[f]) {
      process.stderr.write([
        'There is a different between typings.json/files and source/*.d.ts files',
        listedFiles[f],
        actualFiles[f],
        ''
      ].join('\n'));
      process.exit(1);
    }
  }

})();
