declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      tableDrag?: IBehavior;

    }

    export interface ITheme {

      /**
       * Markup for the marker.
       */
      (func: 'tableDragChangedMarker'): string;

      /**
       * Markup for the indentation.
       */
      (func: 'tableDragIndentation'): string;

      /**
       * Markup for the warning.
       */
      (func: 'tableDragChangedWarning'): string;

    }

    export interface ITableDragStatic {

      /**
       * @param table
       *   DOM object for the table to be made draggable.
       * @param tableSettings
       *   Settings for the table added via drupal_add_dragtable().
       */
      new (
        table: HTMLElement,
        tableSettings: ITableDragOptions
      ): ITableDrag;

      /**
       * The key is a HTML ID without the leading '#' character.
       */
      [id: string]: ITableDrag;

    }

    /**
     * Provides table and field manipulation.
     */
    export interface ITableDrag {

      $table: JQuery;

      table: HTMLElement;

      tableSettings: ITableDragOptions;

      /**
       * Used to hold information about a current drag operation.
       */
      dragObject?: HTMLElement;

      /**
       * Provides operations for row manipulation.
       */
      rowObject?: HTMLElement;

      /**
       * Remember the previous element.
       */
      oldRowElement?: HTMLElement;

      /**
       * Used to determine up or down direction from last mouse move.
       */
      oldY: number;

      /**
       * Whether anything in the entire table has changed.
       */
      changed: boolean;

      /**
       * Maximum amount of allowed parenting.
       */
      maxDepth: number;

      /**
       * Direction of the table.
       */
      rtl: number;

      striping: boolean;

      /**
       * @todo Move this type definition into an interface.
       */
      scrollSettings: {

        amount: number;

        interval: number;

        trigger: number;

      };

      scrollInterval?: number;

      scrollY?: number;

      windowHeight?: number;

      /**
       * Check this table's settings for parent relationships.
       *
       * For efficiency, large sections of code can be skipped if we don't
       * need to track horizontal movement and indentations.
       */
      indentEnabled: number;

      /**
       * Total width of indents, set in makeDraggable.
       */
      indentCount: number;

      indentAmount: number;

      row: ITableDragRow;

      /**
       * Initialize columns containing form elements to be hidden by default.
       *
       * Identify and mark each cell with a CSS class so we can easily
       * toggle show/hide it. Finally, hide columns if user does not have
       * a 'Drupal.tableDrag.showWeight' localStorage value.
       */
      initColumns(): void ;

      addColspanClass(columnIndex: number): () => void;

      displayColumns(displayWeight: boolean): void;

      toggleColumns(): void;

      hideColumns(): void;

      showColumns(): void;

      /**
       * Find the target used within a particular row and group.
       *
       * @param group
       *   Group selector.
       * @param row
       *   The row HTML element.
       *
       * @return
       *   The table row settings.
       */
      rowSettings(
        group: string,
        row: HTMLElement
      ): ITableDragOptions;

      /**
       * Take an item and add event handlers to make it become draggable.
       *
       * @param item
       *   The item to add event handlers to.
       */
      makeDraggable(item: HTMLElement | string): void;

      /**
       * Pointer event initiator, creates drag object and information.
       *
       * @param event
       *   The event object that trigger the drag.
       * @param self
       *   The drag handle.
       * @param item
       *   The item that that is being dragged.
       */
      dragStart(
        event: JQueryEventObject,
        self: ITableDrag,
        item: HTMLElement
      ): void;

      /**
       * Pointer movement handler, bound to document.
       *
       * @param event
       *   The pointer event.
       * @param self
       *   The tableDrag instance.
       *
       * @return boolean | void
       *   Undefined if no dragObject is defined, false otherwise.
       */
      dragRow(
        event: JQueryEventObject,
        self: ITableDrag
      ): void;

      /**
       * Pointerup behavior.
       *
       * @param event
       *   The pointer event.
       * @param self
       *   The tableDrag instance.
       */
      dropRow(
        event: JQueryEventObject,
        self: ITableDrag
      ): void;

      /**
       * Get the coordinates from the event (allowing for browser differences).
       *
       * @param event
       *   The pointer event.
       *
       * @return
       *   An object with `x` and `y` keys indicating the position.
       */
      pointerCoords(event: JQueryEventObject): IPoint;

      /**
       * Get the event offset from the target element.
       *
       * Given a target element and a pointer event, get the event offset
       * from that element. To do this we need the element's position and
       * the target position.
       *
       * @param target
       *   The target HTML element.
       * @param event
       *   The pointer event.
       *
       * @return
       *   An object with `x` and `y` keys indicating the position.
       */
      getPointerOffset(
        target: HTMLElement,
        event: JQueryEventObject
      ): IPoint;

      /**
       * Find the row the mouse is currently over.
       *
       * This row is then taken and swapped with the one being dragged.
       *
       * @param x
       *   The x coordinate of the mouse on the page (not the screen).
       * @param y
       *   The y coordinate of the mouse on the page (not the screen).
       *
       * @return
       *   The drop target row, if found.
       */
      findDropTargetRow(
        x: number,
        y: number
      ): HTMLElement;

      /**
       * After the row is dropped, update the table fields.
       *
       * @param changedRow
       *   DOM object for the row that was just dropped.
       */
      updateFields(changedRow: HTMLElement): void;

      /**
       * After the row is dropped, update a single table field.
       *
       * @param changedRow
       *   DOM object for the row that was just dropped.
       * @param group
       *   The settings group on which field updates will occur.
       */
      updateField(
        changedRow: HTMLElement,
        group: string
      ): void;

      /**
       * Copy all tableDrag related classes from one row to another.
       *
       * Copy all special tableDrag classes from one row's form elements
       * to a different one, removing any special classes that the
       * destination row may have had.
       *
       * @param sourceRow
       *   The element for the source row.
       * @param targetRow
       *   The element for the target row.
       * @param group
       *   The group selector.
       */
      copyDragClasses(
        sourceRow: HTMLElement,
        targetRow: HTMLElement,
        group: string
      ): void;

      /**
       * Check the suggested scroll of the table.
       *
       * @param cursorY
       *   The Y position of the cursor.
       *
       * @return
       *   The suggested scroll.
       */
      checkScroll(cursorY: number): number;

      /**
       * Set the scroll for the table.
       *
       * @param scrollAmount
       *   The amount of scroll to apply to the window.
       */
      setScroll(scrollAmount: number): void;

      /**
       * Command to restripe table properly.
       */
      restripeTable(): void;

      /**
       * Stub function. Allows a custom handler when a row begins dragging.
       *
       * @return
       *   Returns null when the stub function is used.
       */
      onDrag(): any;

      /**
       * Stub function. Allows a custom handler when a row is dropped.
       *
       * @return
       *   Returns null when the stub function is used.
       */
      onDrop(): any;

    }

    export interface ITableDragRow {

      element: HTMLElement;

      method: string;

      group: HTMLElement[];

      groupDepth: number;

      changed: boolean;

      indentEnabled: boolean;

      maxDepth: number;

      /**
       * Allowed values are: down, up.
       */
      direction: string;

      indents: number;

      children: HTMLElement[];

      interval: IRange;

      /**
       * Constructor to make a new object to manipulate a table row.
       *
       * @param tableRow
       *   The DOM element for the table row we will be manipulating.
       * @param method
       *   The method in which this row is being moved. Either 'keyboard'
       *   or 'mouse'.
       * @param indentEnabled
       *   Whether the containing table uses indentations. Used for
       *   optimizations.
       * @param maxDepth
       *   The maximum amount of indentations this row may contain.
       * @param addClasses
       *   Whether we want to add classes to this row to indicate child
       *   relationships.
       */
      new (
        tableRow: HTMLElement,
        method: string,
        indentEnabled: boolean,
        maxDepth: number,
        addClasses: boolean
      ): ITableDragRow;

      /**
       * Find all children of rowObject by indentation.
       *
       * @param addClasses
       *   Whether we want to add classes to this row to indicate child
       *   relationships.
       *
       * @return
       *   An array of children of the row.
       */
      findChildren(addClasses: boolean): HTMLElement[];

      /**
       * Ensure that two rows are allowed to be swapped.
       *
       * @param row
       *   DOM object for the row being considered for swapping.
       *
       * @return
       *   Whether the swap is a valid swap or not.
       */
      isValidSwap(row: HTMLElement): boolean;

      /**
       * Perform the swap between two rows.
       *
       * @param position
       *   Whether the swap will occur 'before' or 'after' the given row.
       * @param row
       *   DOM element what will be swapped with the row group.
       */
      swap(
        position: string,
        row: HTMLElement
      ): void;

      /**
       * Determine the valid indentations interval for the row at a given position.
       *
       * @param prevRow
       *   DOM object for the row before the tested position
       *   (or null for first position in the table).
       * @param nextRow
       *   DOM object for the row after the tested position
       *   (or null for last position in the table).
       *
       * @return
       *   An object with the keys `min` and `max` to indicate the valid
       *   indent interval.
       */
      validIndentInterval(
        prevRow?: HTMLElement,
        nextRow?: HTMLElement
      ): Core.IRange;

      /**
       * Indent a row within the legal bounds of the table.
       *
       * @param indentDiff
       *   The number of additional indentations proposed for the row
       *   (can be positive or negative). This number will be adjusted to
       *   nearest valid indentation level for the row.
       *
       * @return
       *   The number of indentations applied.
       */
      indent(indentDiff: number): number;

      /**
       * Find all siblings for a row.
       *
       * According to its subgroup or indentation. Note that the passed-in row is
       * included in the list of siblings.
       *
       * @param rowSettings
       *   The field settings we're using to identify what constitutes a sibling.
       *
       * @return
       *   An array of siblings.
       */
      findSiblings(rowSettings: ITableDragOptions): HTMLElement[];

      /**
       * Remove indentation helper classes from the current row group.
       */
      removeIndentClasses(): void;

      /**
       * Add an asterisk or other marker to the changed row.
       */
      markChanged(): void;

      /**
       * Stub function. Allows a custom handler when a row is indented.
       *
       * @return
       *   Returns null when the stub function is used.
       */
      onIndent(): any;

      /**
       * Stub function. Allows a custom handler when a row is swapped.
       *
       * @param swappedRow
       *   The element for the swapped row.
       *
       * @return
       *   Returns null when the stub function is used.
       */
      onSwap(swappedRow: HTMLElement): any;

    }

    /**
     * Key tableId.group.tableDragId: ITableDragOptions.
     */
    export type TableDragSettings = Core.ListStr<Core.ListStr<Core.ListNum<Core.ITableDragOptions>>>;

    export interface ITableDragOptions {

      target: string;

      source: string;

      relationship: string;

      action: string;

      hidden: boolean;

      limit: number;

    }

  }

  export interface IDrupalSettings {

    tableDrag?: Core.TableDragSettings;

  }

  export interface IDrupalStatic {

    /**
     * Provides table and field manipulation.
     */
    tableDrag?: Core.ITableDragStatic;

  }

}
