declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      /**
       * Render "new" and "updated" node indicators, as well as "X new" replies links.
       */
      trackerHistory?: IBehavior;

    }

  }

}
