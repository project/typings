(function (
  drop: drupal.IDrupalStatic
): void {

  let $element: JQuery = $('.foo');
  let tm: drupal.Core.ITabbingManager = new drop.tabbingManager();
  let tc: drupal.Core.ITabbingContext = drop.tabbingManager.constraint($element);

  tc.activate();
  tc.deactivate();
  tc.release();

  tm.stack.push(tc);
  tm.activate(tc);
  tm.deactivate(tc);
  tm.recordTabindex($element, 42);
  tm.release();
  tm.restoreTabindex($element, 42);

}(Drupal));
