(function (
  $: JQueryStatic,
  drop: drupal.IDrupalStatic
): void {

  let behavior: drupal.Core.IBehavior = drop.behaviors.tableHeader;

  let table: HTMLElement = $('table').get(0);
  drop.TableHeader.tables.push(new drop.TableHeader(table));

  let $jq: JQuery = $('foo');
  let th: drupal.Core.ITableHeader = drop.TableHeader.tables[0];

  th.$originalTable = $jq;
  th.$originalHeader = $jq;
  th.displayWeight = true;
  th.tableHeight = 42;
  th.tableOffset = 42;
  th.minHeight = 42;
  th.stickyVisible = false;
  th.$stickyTable = $jq;

  th.createSticky();
  $jq = th.stickyPosition(42, 42);
  let bool: boolean = th.checkStickyVisible();

  th.onScroll = function (event: JQueryEventObject): void {
    th.recalculateSticky(event);
  };
})(jQuery, Drupal);
