(function (
  $: JQueryStatic,
  drop: drupal.IDrupalStatic,
  drupalSettings: drupal.IDrupalSettings
) : void {

  let behavior: drupal.Core.IBehavior;

  behavior = drop.behaviors.commentFieldsetSummaries;
  behavior = drop.behaviors.commentByViewer;
  behavior = drop.behaviors.commentNewIndicator;
  behavior = drop.behaviors.nodeNewCommentsLink;

  drupalSettings.comment.newCommentsLinks = {
    'node': {
      'field_foo': {
        42: {
          first_new_comment_link: 'foo',
          new_comment_count: 42
        }
      }
    }
  };

}(jQuery, Drupal, drupalSettings));
