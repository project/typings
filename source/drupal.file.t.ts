(function (
  drop: drupal.IDrupalStatic,
  drupalSettings: drupal.IDrupalSettings
): void {

  let behavior: drupal.Core.IBehavior;

  behavior = drop.behaviors.fileAutoUpload;
  behavior = drop.behaviors.fileButtons;
  behavior = drop.behaviors.filePreviewLinks;
  behavior = drop.behaviors.fileValidateAutoAttach;

  let jQueryEventObject: JQueryEventObject;
  drop.file.disableFields(jQueryEventObject);
  drop.file.openInNewWindow(jQueryEventObject);
  drop.file.progressBar(jQueryEventObject);
  drop.file.triggerUploadButton(jQueryEventObject);
  drop.file.validateExtension(jQueryEventObject);

  let str: string;
  str = 'foo';
  drupalSettings.file.elements[str] = str;

})(Drupal, drupalSettings);
