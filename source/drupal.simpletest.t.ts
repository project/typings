(function (
  drop: drupal.IDrupalStatic,
  drupalSettings: drupal.IDrupalSettings
): void {

  'use strict';

  let behavior: drupal.Core.IBehavior;

  behavior = drop.behaviors.simpleTestGroupCollapse;
  behavior = drop.behaviors.simpleTestSelectAll;
  behavior = drop.behaviors.simpletestTableFilterByText;

  drupalSettings.simpletest.images.push('foo');

}(Drupal, drupalSettings));
