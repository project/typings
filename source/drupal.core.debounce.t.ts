(function (
  drop: drupal.IDrupalStatic
): void {

  let callback: {(...args: any[]): any} = function (...args: any[]): any {
    // Empty block.
  };

  let result: Function = drop.debounce(callback, 42, true);

}(Drupal));
