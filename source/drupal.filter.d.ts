declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      filterGuidelines?: IBehavior;

      filterStatus?: IBehavior;

    }

  }

}
