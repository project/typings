declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      /**
       * Attach handlers to evaluate the strength of any password fields and to
       * check that its confirmation is correct.
       */
      password?: IBehavior;

      permissions?: User.IPermissionsBehavior;

    }

  }

  export namespace User {

    export interface IPermissionsBehavior extends Core.IBehavior {

      /**
       * Toggles all dummy checkboxes based on the checkboxes' state.
       *
       * If the "authenticated user" checkbox is checked, the checked and
       * disabled checkboxes are shown, the real checkboxes otherwise.
       */
      toggle(): void;

    }

    export interface IPasswordStrengthTranslations {

      showStrengthIndicator: boolean;

      strengthTitle: string;

      confirmFailure: string;

      confirmSuccess: string;

      confirmTitle: string;

      username: string;

      tooShort: string;

      addLowerCase: string;

      addUpperCase: string;

      addNumbers: string;

      addPunctuation: string;

      sameAsUsername: string;

      weak: string;

      fair: string;

      good: string;

      strong: string;

      hasWeaknesses: string;

    }

    export interface IPasswordStrengthReport {

      strength: number;

      message: string;

      indicatorText: string;

      indicatorClass: string;

    }

  }

  export interface IDrupalSettings {

    password?: User.IPasswordStrengthTranslations;

  }

  export interface IDrupalStatic {

    /**
     * Evaluate the strength of a user's password.
     *
     * Returns the estimated strength and the relevant output message.
     *
     * @param password
     *   The password to evaluate.
     * @param translate
     *   An object containing the text to display for each strength level.
     *
     * @return
     *   An object containing strength, message, indicatorText and indicatorClass.
     */
    evaluatePasswordStrength?(
      password: string,
      translate: User.IPasswordStrengthTranslations
    ): User.IPasswordStrengthReport;

  }

}
