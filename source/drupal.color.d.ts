declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      color?: IBehavior;

    }

    export interface IColor {

      /**
       * The callback for when the color preview has been attached.
       *
       * @param context
       *   The context to initiate the color behaviour.
       * @param settings
       *   Settings for the color functionality.
       * @param form
       *   The form to initiate the color behaviour on.
       * @param farb
       *   The farbtastic object.
       * @param height
       *   Height of gradient.
       * @param width
       *   Width of gradient.
       */
      callback(
        context: HTMLElement,
        settings: IColorSettings,
        form: HTMLFormElement,
        // @todo Proper object definition.
        farb: Object,
        height: number,
        width: number
      ): void;

    }

    export interface IColorSettings {

      gradients: Core.ListStr<IColorGradient>;

    }

    export interface IColorGradient {

      colors: string[];

      vertical: boolean;

    }

  }

  export interface IDrupalStatic {

    color?: Core.IColor;

  }

}
