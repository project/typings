(function (
  drop: drupal.IDrupalStatic
): void {

  let behavior: drupal.Core.IBehavior;

  behavior = drop.behaviors.nodeDetailsSummaries;
  behavior = drop.behaviors.nodePreviewDestroyLinks;
  behavior = drop.behaviors.nodePreviewSwitchViewMode;
  behavior = drop.behaviors.contentTypes;

  let str: string;
  str = drop.theme('nodePreviewModal');

})(Drupal);
