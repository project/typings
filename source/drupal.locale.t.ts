(function (
  drop: drupal.IDrupalStatic
): void {

  let behavior: drupal.Core.IBehavior;
  let str: string;

  behavior = drop.behaviors.localeDatepicker;
  behavior = drop.behaviors.localeTranslateDirty;
  behavior = drop.behaviors.hideUpdateInformation;

  str = drop.theme('localeTranslateChangedMarker');
  str = drop.theme('localeTranslateChangedWarning');

})(Drupal);
