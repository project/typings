(function (
  drop: drupal.IDrupalStatic
): void {

  let behavior: drupal.Core.IBehavior;

  behavior = drop.behaviors.blockSettingsSummary;
  behavior = drop.behaviors.blockDrag;
  behavior = drop.behaviors.blockFilterByText;
  behavior = drop.behaviors.blockHighlightPlacement;

}(Drupal));
