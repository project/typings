declare namespace drupal {

  export namespace History {

    export interface IDrupalSettings {

      lastReadTimestamps?: Core.ListNum<number>;

      nodesToMarkAsRead?: Core.ListNum<boolean>;

    }

    export interface IDrupalStatic {

      /**
       * Fetch "last read" timestamps for the given nodes.
       *
       * @param nodeIDs
       *   An array of node IDs.
       * @param callback
       *   A callback that is called after the requested timestamps were fetched.
       */
      fetchTimestamps(
        nodeIDs: number[],
        callback: Function
      ): void;

      /**
       * Get the last read timestamp for the given node.
       *
       * @param nodeID
       *   A node ID.
       *
       * @return
       *   A UNIX timestamp.
       */
      getLastRead(nodeID: number): number;

      /**
       * Marks a node as read, store the last read timestamp client-side.
       *
       * @param nodeID
       *   A node ID.
       */
      markAsRead(nodeID: number): void;

      /**
       * Determines whether a server check is necessary.
       *
       * Any content that is >30 days old never gets a "new" or "updated"
       * indicator. Any content that was published before the oldest known reading
       * also never gets a "new" or "updated" indicator, because it must've been
       * read already.
       *
       * @param nodeID
       *   A node ID.
       * @param contentTimestamp
       *   The time at which some content (e.g. a comment) was published.
       *
       * @return
       *   Whether a server check is necessary for the given node and its
       *   timestamp.
       */
      needsServerCheck(
        nodeID: number,
        contentTimestamp: number
      ): boolean;

    }

  }

  export interface IDrupalSettings {

    history?: History.IDrupalSettings;

  }

  export interface IDrupalStatic {

    history?: History.IDrupalStatic;

  }

}
