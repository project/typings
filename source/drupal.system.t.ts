(function (
  drop: drupal.IDrupalStatic,
  drupalSettings: drupal.IDrupalSettings
): void {

  'use strict';

  let behavior: drupal.Core.IBehavior;
  let fieldCopyBehavior: drupal.System.ICopyFieldValueBehavior;
  let jQueryEvent: JQueryEventObject;
  let str: string = 'foo';

  fieldCopyBehavior = drop.behaviors.copyFieldValue;
  fieldCopyBehavior.valueSourceBlurHandler(jQueryEvent);
  fieldCopyBehavior.valueTargetCopyHandler(jQueryEvent, str);

  behavior = drop.behaviors.tableFilterByText;
  behavior = drop.behaviors.dateFormat;

  drupalSettings.copyFieldValue[str] = ['foo', 'bar'];

})(Drupal, drupalSettings);
