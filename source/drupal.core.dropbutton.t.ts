(function (
  $: JQueryStatic,
  drop: drupal.IDrupalStatic
): void {

  let myDropButton: drupal.Core.IDropButton = new drop.DropButton(
    $('.my-drop-button'),
    {
      title: 'Foo'
    }
  );

  let themedDropButton: string = drop.theme('dropbuttonToggle', {title: 'Foo'});

  drop.DropButton.dropbuttons.push(myDropButton);
  myDropButton.toggle(true);
  myDropButton.hoverIn();
  myDropButton.hoverOut();
  myDropButton.focusIn();
  myDropButton.focusOut();
  myDropButton.open();
  myDropButton.close();

}(jQuery, Drupal));
