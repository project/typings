(function (
  drop: drupal.IDrupalStatic
): void {

  let behavior: drupal.Core.IBehavior = drop.behaviors.verticalTabs;
  let vts: drupal.Core.IVerticalTabSettings = {title: 'foo'};
  vts = drop.theme('verticalTab', vts);
  let vt: drupal.Core.IVerticalTab = new drop.verticalTab(vts);

  vt.focus();
  vt.updateSummary();
  vt = vt.tabShow();
  vt = vt.tabHide();

})(Drupal);
