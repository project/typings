(function (
  $: JQueryStatic,
  drop: drupal.IDrupalStatic,
  drupalSettings: drupal.IDrupalSettings
): void {

  'use strict';

  let behavior: drupal.Core.IBehavior;
  behavior = drop.behaviors.ViewsAjaxView;
  behavior.attach();

  let myString: string = 'foo';
  let myBool: boolean;
  let assoc: {[key: string]: string};
  let htmlElement: HTMLElement = document.getElementById('footer');
  let viewArgs: drupal.Views.IViewArgs;

  viewArgs = drop.Views.parseViewArgs(myString, myString);
  viewArgs.view_args = myString;
  viewArgs.view_path = myString;
  myString = drop.Views.getPath(myString);
  assoc = drop.Views.parseQueryString(myString);
  myString = drop.Views.pathPortionfunction(assoc[myString]);

  let ajaxViewSettings: drupal.Views.IAjaxViewSettings;
  ajaxViewSettings = {
    view_dom_id: myString,
    view_base_path: myString,
    view_name: myString,
    view_display_id: myString
  };

  drupalSettings.views.ajaxViews[myString] = ajaxViewSettings;

  let ajaxView: drupal.Views.IAjaxView;
  ajaxView = new drop.views.ajaxView(ajaxViewSettings);
  drop.views.instances.push(ajaxView);

  ajaxView.exposedFormAjax = '@todo';
  ajaxView.$view = $('.view');
  ajaxView.element_settings = {
    url: 'foo',
    submit: ajaxViewSettings,
    setClick: true,
    event: 'click',
    selector: '.foo',
    progress: {
      type: 'fullScreen'
    }
  };
  ajaxView.settings = ajaxViewSettings;
  ajaxView.$exposed_form = $('.exposed-form');
  ajaxView.refreshViewAjax = '@todo';
  ajaxView.attachExposedFormAjax();
  myBool = ajaxView.filterNestedViews();
  ajaxView.attachPagerAjax();
  ajaxView.attachPagerLinkAjax(myBool.toString(), htmlElement);
  ajaxView.viewsScrollTop();

})(jQuery, Drupal, drupalSettings);
