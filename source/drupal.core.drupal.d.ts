declare namespace drupal {

  export namespace Core {

    export type ListStr<T> = {

      [key: string]: T;

    }

    export type ListNum<T> = {

      [key: number]: T;

    }

    export interface IBehavior {

      attach(
        context?: HTMLElement,
        settings?: IDrupalSettings
      ): void;

      detach?(
        context?: HTMLElement,
        settings?: IDrupalSettings,
        trigger?: string
      ): void;

    }

    export interface IBehaviors {
    }

    export interface ITranslationOptions {

      context?: string;

    }

    export interface IPlaceholders {
      [key: string]: string;
    }

    export interface ITheme {

      (func: 'placeholder', str: string): string;

      (func: string, ...params: any[]): string;

    }

    export interface IPoint {

      x: number;

      y: number;

    }

    export interface IRange {

      min: number;

      max: number;

    }

    export interface IOffsets {

      top: number;

      right: number;

      bottom: number;

      left: number;

    }

    export interface IUrlGenerator {

      (path: string): string;

      toAbsolute(url: string): string;

      isLocal(url: string): boolean;

    }

    export interface ILocale {
    }
  }

  export interface IDrupalSettings {

    // @todo Move into an Interface.
    path: {

      baseUrl: string;

      currentLanguage: string;

      currentPath: string;

      currentPathIsAdmin: string;

      isFront: boolean;

      pathPrefix: string;

      scriptPath: string;

    };

    pluralDelimiter: string;

    // @todo Move into an Interface.
    user: {

      uid: number;

      permissionsHash: string;

    };

    // @todo Move into an Interface.
    jquery?: {

      ui?: {
        datepicker?: JQueryUI.DatepickerOptions;
      }

    };

  }

  export interface IDrupalStatic {

    attachBehaviors(
      context: HTMLElement,
      settings: IDrupalSettings
    ): void;

    detachBehaviors(
      context?: HTMLElement,
      settings?: IDrupalSettings,
      trigger?: string
    ): void;

    behaviors: Core.IBehaviors;

    locale: Core.ILocale;

    checkPlain(text: string): string;

    // @todo Remove any.
    checkWidthBreakpoint?(width: number): any;

    // @todo Remove any.
    encodePath(item: any): any;

    formatPlural(
      count: number,
      singular: string,
      plural: string,
      args?: Core.IPlaceholders,
      options?: Core.ITranslationOptions
    ): string;

    formatString(
      str: string,
      args: Core.IPlaceholders
    ): string;

    stringReplace(
      str: string,
      args?: Core.IPlaceholders,
      keys?: string[]
    ): string;

    t(
      str: string,
      args?: Core.IPlaceholders,
      options?: Core.ITranslationOptions
    ): string;

    theme: Core.ITheme;

    throwError(error: Error): void;

    url: Core.IUrlGenerator;

  }
}

/* tslint:disable:interface-name */
interface Window {

  Drupal?: drupal.IDrupalStatic;

  drupalSettings?: drupal.IDrupalSettings;

}
/* tslint:enable:interface-name */

declare var drupalSettings: drupal.IDrupalSettings;
declare var Drupal: drupal.IDrupalStatic;
