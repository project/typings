declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      /**
       * This script transforms a set of details into a stack of vertical tabs.
       *
       * Each tab may have a summary which can be updated by another
       * script. For that to work, each details element has an associated
       * 'verticalTabCallback'
       * (with jQuery.data() attached to the details), which is called
       * every time the user performs an update to a form element inside
       * the tab pane.
       */
      verticalTabs?: IBehavior;

    }

    export interface ITheme {

      /**
       * Theme function for a vertical tab.
       */
      (func: 'verticalTab', settings: IVerticalTabSettings): IVerticalTabSettings;

      (func: string, ...params: any[]): IVerticalTabSettings;

    }

    export interface IVerticalTabSettings {

      item?: JQuery;

      title: string;

      summary?: JQuery;

      details?: JQuery;

      link?: JQuery;

    }

    /**
     * The vertical tab object represents a single tab within a tab group.
     */
    export interface IVerticalTab extends IVerticalTabSettings {

      new (settings: IVerticalTabSettings): IVerticalTab;

      /**
       * Displays the tab's content pane.
       */
      focus(): void;

      /**
       * Updates the tab's summary.
       */
      updateSummary(): void;

      /**
       * Shows a vertical tab pane.
       *
       * @return
       *   The verticalTab instance.
       */
      tabShow(): IVerticalTab;

      /**
       * Hides a vertical tab pane.
       *
       * @return
       *   The verticalTab instance.
       */
      tabHide(): IVerticalTab;

    }

  }

  export interface IDrupalSettings {

    /**
     * @todo Move this to the right place.
     */
    widthBreakpoint?: number;

  }

  export interface IDrupalStatic {

    verticalTab?: Core.IVerticalTab;

  }

}
