(function (
  $: JQueryStatic,
  drop: drupal.IDrupalStatic
): void {

  let behavior: drupal.Core.IBehavior;

  behavior = drop.behaviors.color;

  let htmlElement: HTMLElement = $('.foo').get(0);
  let colorSettings: drupal.Core.IColorSettings;
  let form: HTMLFormElement = <HTMLFormElement> $('form').get(0);
  let farb: Object = {};
  let height: number = 42;
  let width: number = 42;

  colorSettings = {
    gradients: {
      'foo': {
        colors: ['a', 'b'],
        vertical: true
      }
    }
  };
  drop.color.callback(
    htmlElement,
    colorSettings,
    form,
    farb,
    height,
    width
  );

}(jQuery, Drupal));
