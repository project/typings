declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      /**
       * Attaches sticky table headers.
       */
      tableHeader?: IBehavior;

    }

    export interface ITableHeaderStatic {

      tables: ITableHeader[];

      /**
       * @param table
       *   DOM object for the table to add a sticky header to.
       */
      new (table: HTMLElement): ITableHeader;

    }

    /**
     * Provides sticky table headers.
     *
     * TableHeader will make the current table header stick to the top of
     * the page if the table is very long.
     */
    export interface ITableHeader {

      $originalTable: JQuery;

      $originalHeader: JQuery;

      $originalHeaderCells: JQuery;

      displayWeight: boolean;

      /**
       * Absolute position of the table on the page.
       */
      tableHeight: number;

      /**
       * Absolute position of the table on the page.
       */
      tableOffset: number;

      /**
       * Minimum height in pixels for the table to have a sticky header.
       */
      minHeight: number;

      /**
       * Boolean storing the sticky header visibility state.
       */
      stickyVisible: boolean;

      $stickyTable: JQuery;

      /**
       * Create the duplicate header.
       */
      createSticky(): void;

      /**
       * Set absolute position of sticky.
       *
       * @param offsetTop
       *   The top offset for the sticky header.
       * @param offsetLeft
       *   The left offset for the sticky header.
       *
       * @return
       *   The sticky table as a jQuery collection.
       */
      stickyPosition(offsetTop: number,
                     offsetLeft: number): JQuery;

      /**
       * Returns true if sticky is currently visible.
       *
       * @return
       *   The visibility status.
       */
      checkStickyVisible(): boolean;

      /**
       * Check if sticky header should be displayed.
       *
       * This function is throttled to once every 250ms to avoid unnecessary
       * calls.
       *
       * @param event
       *   The scroll event.
       */
      onScroll(event: JQueryEventObject): void;

      /**
       * Event handler: recalculates position of the sticky table header.
       *
       * @param event
       *   Event being triggered.
       */
      recalculateSticky(event: JQueryEventObject): void;

    }

  }

  export interface IDrupalStatic {

    TableHeader: Core.ITableHeaderStatic;

  }

}
