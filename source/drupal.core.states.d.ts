declare namespace drupal {

  export namespace Core {

    export interface IStatesStatic {

      Trigger: IStatesTriggerStatic;

      State: IStateStatic;

      new (): IStates;

    }

    export interface IStates {

      /**
       * An array of functions that should be postponed.
       */
      postponed: (() => void)[];

      /**
       * Object representing an element that depends on other elements.
       */
      Dependent?: IDependent;

      State: IStateStatic;

    }

    export interface IStatesTriggerStatic {

      /**
       * This list of states contains functions that are used to monitor
       * the state of an element. Whenever an element depends on the state
       * of another element, one of these trigger functions is added to
       * the dependee so that the dependent element can be updated.
       */
      states: {

        empty: IStatesTriggerStateEmpty;

        checked: IStatesTriggerStateChecked;

        value: IStatesTriggerStateValue;

        collapsed: IStatesTriggerStateCollapsed;

      };

      new (args: IStatesTriggerOptions): IStatesTrigger;

    }

    export interface IStatesTrigger extends IStatesTriggerOptions {

      element: HTMLElement;

      initialize(): void;

      defaultTrigger(
        event: JQueryEventObject,
        valueFn: () => any
      ): void;

    }

    export interface IStateStatic {

      /**
       * This list of aliases is used to normalize states and associates
       * negated names with their respective inverse state.
       */
      aliases: Core.ListStr<string>;

      /**
       * Creates a new State object by sanitizing the passed value.
       */
      sanitize(name: string | IState): IState;

      State: IState;

      /**
       * A state object is used for describing the state and performing aliasing.
       */
      new (name: string): IState;

    }

    export interface IState {

      name: string;

      pristine: string;

      invert: boolean;

      /**
       * A state object is used for describing the state and performing aliasing.
       */
      new (name: string): IState;

    }

    export interface IStatesTriggerOptions {

      /**
       * Name of the state.
       */
      state: string;

      selector: string;

    }

    export interface IComparer {

      (
        reference: any,
        value: any
      ): boolean;

    }

    export interface IDependentArgs {

      element: JQuery;

      state: IState;

      constraints: any;

    }

    export interface IDependentComparisons {

      RegExp: IComparer;

      Function: IComparer;

      Number: IComparer;

    }

    export interface IDependent {

      new (args: IDependentArgs): IDependent;

      dependees?: {[selector: string]: string};

      /**
       * Comparison functions for comparing the value of an element with
       * the specification from the dependency settings.
       * If the object type can't be found in this list, the === operator
       * is used by default.
       */
      comparisons?: IDependentComparisons;

      /**
       * Initializes one of the elements this dependent depends on.
       */
      initializeDependee?(
        selector: string,
        dependeeStates: {[stateName: string]: IState}
      ): void;

      /**
       * Compares a value with a reference value.
       */
      compare?(// @todo Remove any.
        reference: any,
        selector: string,
        state: IState
      ): void;

      /**
       * Update the value of a dependee's state.
       */
      update?(
        selector: string,
        state: IState,
        value: string
      ): void;

      /**
       * Triggers change events in case a state changed.
       */
      reevaluate?(): any;

      /**
       * Evaluates child constraints to determine if a constraint is satisfied.
       */
      verifyConstraints?(// @todo Remove any.
        constraints: any,
        selector: string
      ): boolean;

      /**
       * Checks whether the value matches the requirements for this constraint.
       */
      checkConstraints(// @todo Remove any.
        value: any,
        selector: string,
        // @todo Remove any.
        state: IState
      ): boolean;

      /**
       * Gathers information about all required triggers.
       *
       * @todo Remove any.
       */
      getDependees?(): any;

      Trigger?: IStatesTrigger;

      State?: IState;

    }

    export interface IStatesTriggerStateEmpty {

      keyup(): boolean;

    }

    export interface IStatesTriggerStateChecked {

      change(): boolean;

    }

    export interface IStatesTriggerStateValue {

      keyup(): boolean;

      change(): boolean;

    }

    export interface IStatesTriggerStateCollapsed {

      collapsed(): boolean;

    }

    export interface IBehaviors {

      states?: IBehavior;

    }

  }

  export interface IDrupalStatic {

    states: Core.IStatesStatic;

  }

}
