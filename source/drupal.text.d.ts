declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      /**
       * Auto-hide summary textarea if empty and show hide and unhide links.
       */
      textSummary?: IBehavior;

    }

  }

}
