declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      /**
       * Attaches language support to the jQuery UI datepicker component.
       */
      localeDatepicker?: IBehavior;

      /**
       * Marks changes of translations.
       */
      localeTranslateDirty?: IBehavior;

      /**
       * Show/hide the description details on Available translation updates page.
       */
      hideUpdateInformation?: IBehavior;

    }

    export interface ITheme {

      /**
       * Creates markup for a changed translation marker.
       *
       * @return
       *   Markup for the marker.
       */
      (func: 'localeTranslateChangedMarker'): string;

      /**
       * Creates markup for the translation changed warning.
       *
       * @return
       *   Markup for the warning.
       */
      (func: 'localeTranslateChangedWarning'): string;

    }

  }

}
