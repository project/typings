declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      /**
       * Move a block in the blocks table from one region to another.
       *
       * This behavior is dependent on the tableDrag behavior, since it uses the
       * objects initialized in that behavior to update the row.
       */
      termDrag?: IBehavior;

    }

  }

  export namespace Taxonomy {

    export interface IDrupalSettings {

      backStep: number;

      forwardStep: number;

    }

  }

  export interface IDrupalSettings {

    taxonomy?: Taxonomy.IDrupalSettings;

  }

}
