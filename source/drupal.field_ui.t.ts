(function (
  $: JQueryStatic,
  drop: drupal.IDrupalStatic,
  drupalSettings: drupal.IDrupalSettings
): void {

  let str: string = 'foo';
  let behavior: drupal.Core.IBehavior;
  let tableDrag: drupal.Core.ITableDrag;
  let htmlTableElement: HTMLTableElement;
  let htmlElement: HTMLElement;
  let namedHTMLElements: {[name: string]: HTMLElement};
  let htmlTableRowElement: HTMLTableRowElement;
  let displayOverviewFieldData: drupal.FieldUI.DisplayOverview.IFieldData;
  let displayOverviewFieldRowHandler: drupal.FieldUI.DisplayOverview.IFieldRowHandler;
  let $j: JQuery;

  behavior = drop.behaviors.fieldUIFieldStorageAddForm;
  behavior = drop.behaviors.fieldUIDisplayOverview;

  htmlTableRowElement = <HTMLTableRowElement>$('tr').get(0);
  displayOverviewFieldData = {
    name: 'foo',
    region: 'foo',
    tableDrag: tableDrag
  };
  displayOverviewFieldRowHandler = new drop.fieldUIDisplayOverview.field(
    htmlTableRowElement,
    displayOverviewFieldData
  );

  tableDrag = displayOverviewFieldRowHandler.tableDrag;
  str = displayOverviewFieldRowHandler.name;
  str = displayOverviewFieldRowHandler.region;
  $j = displayOverviewFieldRowHandler.$pluginSelect;
  str = displayOverviewFieldRowHandler.getRegion();
  namedHTMLElements = displayOverviewFieldRowHandler.regionChange(str);

  namedHTMLElements = {'foo': htmlElement};
  drop.fieldUIOverview.attach(htmlTableElement, {}, drop.fieldUIDisplayOverview);
  drop.fieldUIOverview.onChange();
  drop.fieldUIOverview.onDrop();
  drop.fieldUIOverview.onSwap(htmlElement);
  drop.fieldUIOverview.AJAXRefreshRows(namedHTMLElements);

  htmlTableRowElement = displayOverviewFieldRowHandler.row;
  str = displayOverviewFieldRowHandler.name;
  str = displayOverviewFieldRowHandler.region;
  tableDrag = displayOverviewFieldRowHandler.tableDrag;
  $j = displayOverviewFieldRowHandler.$pluginSelect;
  str = displayOverviewFieldRowHandler.getRegion();
  namedHTMLElements = displayOverviewFieldRowHandler.regionChange(str);

  drupalSettings.existingFieldLabels[str] = 'foo';
  drupalSettings.fieldUIRowsData[str].defaultPlugin = 'foo';
  drupalSettings.fieldUIRowsData[str].name = 'foo';
  drupalSettings.fieldUIRowsData[str].region = 'foo';
  drupalSettings.fieldUIRowsData[str].rowHandler = 'foo';
  drupalSettings.fieldUIRowsData[str].tableDrag = tableDrag;

})(jQuery, Drupal, drupalSettings);
