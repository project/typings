declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      /**
       * Attach behaviors to the file fields passed in the settings.
       */
      fileValidateAutoAttach?: IBehavior;

      /**
       * Attach behaviors to file element auto upload.
       */
      fileAutoUpload?: IBehavior;

      /**
       * Attach behaviors to the file upload and remove buttons.
       */
      fileButtons?: IBehavior;

      /**
       * Attach behaviors to links within managed file elements for preview windows.
       */
      filePreviewLinks?: IBehavior;

    }

  }

  export namespace File {

    export interface IDrupalSettings {

      /**
       * Key-value pairs of jQuery selector and comma separated list of file extensions.
       */
      elements: Core.ListStr<string>;

    }

    export interface IDrupalStatic {

      /**
       * Client-side file input validation of file extensions.
       *
       * @param event
       *   The event triggered. For example `change.fileValidate`.
       */
      validateExtension(event: JQueryEventObject): void;

      /**
       * Trigger the upload_button mouse event to auto-upload as a managed file.
       *
       * @param event
       *   The event triggered. For example `change.autoFileUpload`.
       */
      triggerUploadButton(event: JQueryEventObject): void;

      /**
       * Prevent file uploads when using buttons not intended to upload.
       *
       * @param event
       *   The event triggered, most likely a `mousedown` event.
       */
      disableFields(event: JQueryEventObject): void;

      /**
       * Add progress bar support if possible.
       *
       * @name Drupal.file.progressBar
       *
       * @param event
       *   The event triggered, most likely a `mousedown` event.
       */
      progressBar(event: JQueryEventObject): void;

      /**
       * Open links to files within forms in a new window.
       *
       * @name Drupal.file.openInNewWindow
       *
       * @param event
       *   The event triggered, most likely a `click` event.
       */
      openInNewWindow(event: JQueryEventObject): void;

    }

  }

  export interface IDrupalSettings {

    file?: File.IDrupalSettings;

  }

  export interface IDrupalStatic {

    file?: File.IDrupalStatic;

  }

}
