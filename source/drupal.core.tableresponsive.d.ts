declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      /**
       * Attaches tableResponsive functionality.
       */
      tableResponsive?: IBehavior;

    }

    /**
     * The TableResponsive object optimizes table presentation for screen size.
     *
     * A responsive table hides columns at small screen sizes, leaving the
     * most important columns visible to the end user. Users should not be
     * prevented from accessing all columns, however. This class adds a
     * toggle to a table with hidden columns that exposes the columns.
     * Exposing the columns will likely break layouts, but it provides the
     * user with a means to access data, which is a guiding principle of
     * responsive design.
     */
    export interface ITableResponsiveStatic {

      tables?: ITableResponsive[];

      new (table: HTMLElement): ITableResponsive;

    }

    /**
     * Associates an action link with the table that will show hidden columns.
     *
     * Columns are assumed to be hidden if their header has the class
     * priority-low or priority-medium.
     */
    export interface ITableResponsive {

      table: HTMLElement;

      $table: JQuery;

      $headers: JQuery;

      showText: string;

      $link: JQuery;

      $revealedCells: JQuery;

      /**
       * @param event
       *   The event triggered.
       */
      eventhandlerEvaluateColumnVisibility(event: JQueryEventObject): void;

      /**
       * Toggle the visibility of columns based on their priority.
       *
       * Columns are classed with either 'priority-low' or
       * 'priority-medium'.
       *
       * @param event
       *   The event triggered.
       */
      eventhandlerToggleColumns(event: JQueryEventObject): void;

    }

  }

  export interface IDrupalStatic {

    TableResponsive?: Core.ITableResponsiveStatic;

  }

}
