declare namespace drupal {

  export namespace Core {

    export interface ITabbingManager {

      stack: ITabbingContext[];

      new (): ITabbingManager;

      constraint(elements: JQuery | string): ITabbingContext;

      release(): void;

      activate(tabbingContext: ITabbingContext): void;

      deactivate(tabbingContext: ITabbingContext): void;

      recordTabindex($el: JQuery, level: number): void;

      restoreTabindex($el: JQuery, level: number): void;

    }

    export interface ITabbingContext extends ITabbingContextOptions {

      new (options: ITabbingContextOptions): ITabbingContext;

      release(): void;

      activate(): void;

      deactivate(): void;

    }

    export interface ITabbingContextOptions {

      level?: number;

      $tabbableElements: JQuery;

      $disabledElements: JQuery;

      released: boolean;

      active: boolean;

    }

  }

  export interface IDrupalStatic {

    tabbingManager?: Core.ITabbingManager;

  }

}
