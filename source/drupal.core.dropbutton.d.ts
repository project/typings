declare namespace drupal {

  export namespace Core {

    export interface IDropButtonSettings {

      title: string;

    }

    export interface IDropButton {

      new (
        dropbutton: JQuery,
        settings: IDropButtonSettings
      ): IDropButton;

      dropbuttons: IDropButton[];

      $dropbutton: JQuery;

      $actions: JQuery;

      toggle(show: boolean): void;

      hoverIn(): void;

      hoverOut(): void;

      open(): void;

      close(): void;

      focusIn(): void;

      focusOut(): void;

    }

    export interface ITheme {

      (func: 'dropbuttonToggle', options: IDropButtonSettings): string;

    }

    export interface IBehaviors {

      dropButton?: IBehavior;

    }

  }

  export interface IDrupalStatic {

    DropButton: Core.IDropButton;

  }

}
