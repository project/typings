(function (
  drop: drupal.IDrupalStatic,
  drupalSettings: drupal.IDrupalSettings
): void {

  let bool: boolean = false;
  let nodeId: number = 42;
  let timestamp: number = 84;
  let callback: () => void = function (): void {
    return;
  };

  drupalSettings.history.lastReadTimestamps[nodeId] = timestamp;
  drupalSettings.history.nodesToMarkAsRead[nodeId] = true;

  drop.history.fetchTimestamps([42, 84], callback);
  timestamp = drop.history.getLastRead(nodeId);
  drop.history.markAsRead(nodeId);
  bool = drop.history.needsServerCheck(nodeId, timestamp);

})(Drupal, drupalSettings);
