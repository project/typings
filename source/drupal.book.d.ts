declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      bookDetailsSummaries?: IBehavior;

    }

  }

}
