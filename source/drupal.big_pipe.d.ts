declare namespace drupal {

  export interface IDrupalSettings {

    bigPipePlaceholderIds?: {

      [placeholderId: string]: boolean;

    };

  }

}
