declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      /**
       * Adds behaviors to the field storage add form.
       */
      fieldUIFieldStorageAddForm: IBehavior;

      /**
       * Attaches the fieldUIOverview behavior.
       */
      fieldUIDisplayOverview: IBehavior;

    }

  }

  export namespace FieldUI {

    export interface IFieldInfo {

      defaultPlugin: string;

      name: string;

      region: string;

      rowHandler: string;

      tableDrag: drupal.Core.ITableDrag;

    }

    export interface IOverview {

      /**
       * Attaches the fieldUIOverview behavior.
       *
       * @param table
       *   The table element for the overview.
       * @param rowsData
       *   The data of the rows in the table.
       * @param rowHandlers
       *   Handlers to be added to the rows.
       */
      attach(
        table: HTMLTableElement,
        rowsData: drupal.Core.ListStr<DisplayOverview.IFieldData>,
        rowHandlers: drupal.FieldUI.DisplayOverview.IRowHandlers
      ): void;

      /**
       * Event handler to be attached to form inputs triggering a region change.
       */
      onChange(): void;

      /**
       * Lets row handlers react when a row is dropped into a new region.
       */
      onDrop(): void;

      /**
       * Refreshes placeholder rows in empty regions while a row is being dragged.
       *
       * Copied from block.js.
       *
       * @param draggedRow
       *   The tableDrag rowObject for the row being dragged.
       */
      onSwap(draggedRow: HTMLElement): void;

      /**
       * Triggers Ajax refresh of selected rows.
       *
       * The 'format type' selects can trigger a series of changes in child rows.
       * The #ajax behavior is therefore not attached directly to the selects, but
       * triggered manually through a hidden #ajax 'Refresh' button.
       *
       * @param rows
       *   A hash object, whose keys are the names of the rows to refresh (they
       *   will receive the 'ajax-new-content' effect on the server side), and
       *   whose values are the DOM element in the row that should get an Ajax
       *   throbber.
       */
      AJAXRefreshRows(rows: drupal.Core.ListStr<HTMLElement>): void;

    }

    export namespace DisplayOverview {

      /**
       * {[name: string]: BaseRowHandler}
       */
      export interface IRowHandlers {

        field: IFieldRowHandlerConstructor;

      }

      export interface IRowHandlerConstructor {

        new(
          row: HTMLTableRowElement,
          data: IFieldData
        ): IRowHandler;

      }

      export interface IRowHandler {

        row: HTMLTableRowElement;

        name: string;

        region: string;

        tableDrag: drupal.Core.ITableDrag;

        /**
         * Returns the region corresponding to the current form values of the row.
         *
         * @return
         *   Either 'hidden' or 'content'.
         */
        getRegion(): string;

        /**
         * Reacts to a row being changed regions.
         *
         * This function is called when the row is moved to a different region, as
         * a
         * result of either :
         * - a drag-and-drop action (the row's form elements then probably need to
         * be updated accordingly)
         * - user input in one of the form elements watched by the
         *   {@link Drupal.fieldUIOverview.onChange} change listener.
         *
         * @param region
         *   The name of the new region for the row.
         *
         * @return
         *   A hash object indicating which rows should be Ajax-updated as a result
         *   of the change, in the format expected by
         *   {@link Drupal.fieldUIOverview.AJAXRefreshRows}.
         */
        regionChange(region: string): drupal.Core.ListStr<HTMLElement>;

      }

      export interface IFieldRowHandlerConstructor extends IRowHandlerConstructor {

        new(
          row: HTMLTableRowElement,
          data: IFieldData
        ): IFieldRowHandler;

      }

      export interface IFieldRowHandler extends IRowHandler {

        $pluginSelect: JQuery;

      }

      export interface IFieldData {

        name: string;

        region: string;

        tableDrag: drupal.Core.ITableDrag;

      }

    }

  }

  export interface IDrupalSettings {

    existingFieldLabels: drupal.Core.ListStr<string>;

    fieldUIRowsData: drupal.Core.ListStr<FieldUI.IFieldInfo>;

  }

  export interface IDrupalStatic {

    /**
     * Namespace for the field UI overview.
     */
    fieldUIOverview?: drupal.FieldUI.IOverview;

    /**
     * Row handlers for the 'Manage display' screen.
     */
    fieldUIDisplayOverview?: drupal.FieldUI.DisplayOverview.IRowHandlers;

  }

}
