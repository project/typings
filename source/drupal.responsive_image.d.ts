declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      /**
       * Call picturefill so newly added responsive images are processed.
       */
      responsiveImageAJAX?: IBehavior;

    }

  }

}
