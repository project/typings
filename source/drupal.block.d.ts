declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      blockSettingsSummary?: IBehavior;

      blockDrag?: IBehavior;

      blockFilterByText?: IBehavior;

      blockHighlightPlacement?: IBehavior;

    }

  }

}
