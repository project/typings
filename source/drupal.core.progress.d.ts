declare namespace drupal {

  export namespace Core {

    export interface ITheme {

      (func: 'progressBar', id: string): string;

    }

    export type IProgressBarUpdateCallback = {

      (
        percentage: number,
        message: string,
        progressBar: IProgressBar
      ): void

    };

    export type IProgressBarErrorCallback = {

      (progressBar: IProgressBar): void

    };

    export interface IProgressBar {

      new (
        id: string,
        updateCallback: IProgressBarUpdateCallback,
        method: string,
        errorCallback: IProgressBarErrorCallback
      ): IProgressBar;

      id: string;

      method: string;

      element: HTMLElement;

      delay: number;

      uri: string;

      timer: number;

      updateCallback?: IProgressBarUpdateCallback;

      errorCallback?: IProgressBarErrorCallback;

      setProgress(
        percentage: number,
        message: string,
        label: string
      ): void;

      startMonitoring(
        uri: string,
        delay: number
      ): void;

      stopMonitoring(): void;

      sendPing(): void;

      displayError(str: string): void;

    }

  }

  export interface IDrupalStatic {

    ProgressBar?: Core.IProgressBar;

  }
}
