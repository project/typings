declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      /**
       * Attaches field copy behavior from input fields to other input fields.
       *
       * When a field is filled out, apply its value to other fields that will
       * likely use the same value. In the installer this is used to populate the
       * administrator email address with the same value as the site email address.
       */
      copyFieldValue?: System.ICopyFieldValueBehavior;

      /**
       * Filters the module list table by a text input search string.
       *
       * Additionally accounts for multiple tables being wrapped in "package" details
       * elements.
       *
       * Text search input: input.table-filter-text
       * Target table:      input.table-filter-text[data-table]
       * Source text:       .table-filter-text-source, .module-name, .module-description
       */
      tableFilterByText?: IBehavior;

      /**
       * Display the preview for date format entered.
       */
      dateFormat?: IBehavior;

    }

  }

  export namespace System {

    export interface ICopyFieldValueBehavior extends Core.IBehavior {

      /**
       * Event handler that fill the target element with the specified value.
       *
       * @param event
       *   Event object.
       * @param value
       *   Custom value from jQuery trigger.
       */
      valueTargetCopyHandler(
        event: JQueryEventObject,
        value: string
      ): void;

      /**
       * Handler for a Blur event on a source field.
       *
       * This event handler will trigger a 'value:copy' event on all dependent
       * fields.
       *
       * @param event
       *   The event triggered.
       */
      valueSourceBlurHandler(event: JQueryEventObject): void;

    }

  }

  export interface IDrupalSettings {

    /**
     * Source and target IDs.
     */
    copyFieldValue?: Core.ListStr<string[]>;

  }

}
