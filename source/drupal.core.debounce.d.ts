declare namespace drupal {

  export interface IDrupalStatic {

    // @todo Remove any.
    debounce?(
      func: (...args: any[]) => any,
      wait: number,
      immediate: boolean
    ): Function;

  }

}
