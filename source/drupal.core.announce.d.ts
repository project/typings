declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      drupalAnnounce?: IBehavior;

    }

  }

  export interface IDrupalStatic {

    // @todo Remove any.
    announce?(text: string, priority: string): any;

  }
}
