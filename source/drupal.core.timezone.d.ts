declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      /**
       * Set the client's system time zone as default values of form fields.
       */
      setTimezone?: IBehavior;

    }

  }

}
