(function (
  drupalSettings: drupal.IDrupalSettings
): void {

  let placeholderId: string = 'foo';
  drupalSettings.bigPipePlaceholderIds[placeholderId] = true;

}(drupalSettings));
