/* tslint:disable:interface-name */
interface JQuery {

  drupalGetSummary?(): string;

  drupalSetSummary?(callback: string | ((element: HTMLElement) => string)): JQuery;

}
/* tslint:enable:interface-name */

declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      formSingleSubmit?: IBehavior;

      formUpdated?: IBehavior;

      fillUserInfoFromBrowser?: IBehavior;

    }

  }

}
