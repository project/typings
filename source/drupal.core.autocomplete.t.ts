(function (
  drop: drupal.IDrupalStatic
): void {

  'use strict';

  let key: string = 'foo';
  drop.autocomplete.cache[key] = 'bar';

  let fragments: string[] = drop.autocomplete.splitValues('foo, bar');
  fragments.push(drop.autocomplete.extractLastTerm('foo:bar'));

  drop.autocomplete.options.firstCharacterBlacklist = fragments[0];

}(Drupal));
