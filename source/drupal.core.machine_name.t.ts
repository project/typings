(function (
  drupalSettings: drupal.IDrupalSettings
): void {

  let fieldName: string = 'foo';
  let myMachineName: drupal.Core.IDrupalSettingsMachineNameEntry;
  myMachineName = drupalSettings.machineName[fieldName];
  myMachineName.target = 'Foo';
  myMachineName.suffix = 'Foo';
  myMachineName.label = 'Foo';
  myMachineName.replace_pattern = 'Foo';
  myMachineName.replace = 'Foo';
  myMachineName.standalone = false;
  myMachineName.field_prefix = 'Foo';
  myMachineName.field_suffix = 'Foo';
  myMachineName.maxlength = 42;

}(drupalSettings));
