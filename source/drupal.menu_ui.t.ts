(function (
  drop: drupal.IDrupalStatic
): void {

  let behavior: drupal.Core.IBehavior;

  behavior = drop.behaviors.menuUiDetailsSummaries;
  behavior = drop.behaviors.menuUiLinkAutomaticTitle;
  behavior = drop.behaviors.menuUiChangeParentItems;

  drop.menuUiUpdateParentList();

})(Drupal);
