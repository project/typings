declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      /**
       * Behaviors for tabs in the node edit form.
       */
      nodeDetailsSummaries?: IBehavior;

      /**
       * Disables all non-relevant links in node previews.
       *
       * Destroys links (except local fragment identifiers such as href="#frag") in
       * node previews to prevent users from leaving the page.
       */
      nodePreviewDestroyLinks?: IBehavior;

      /**
       * Switch view mode.
       */
      nodePreviewSwitchViewMode?: IBehavior;

      /**
       * Behaviors for setting summaries on content type form.
       */
      contentTypes?: IBehavior;

    }

    export interface ITheme {

      /**
       * Theme function for node preview modal.
       *
       * @return
       *   Markup for the node preview modal.
       */
      (func: 'nodePreviewModal'): string;

    }

  }

}
