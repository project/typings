declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      /**
       * Collapses table rows followed by group rows on the test listing page.
       */
      simpleTestGroupCollapse?: IBehavior;

      /**
       * Toggles test checkboxes to match the group checkbox.
       */
      simpleTestSelectAll?: IBehavior;

      /**
       * Filters the test list table by a text input search string.
       */
      simpletestTableFilterByText?: IBehavior;

    }

  }

  export namespace SimpleTest {

    export interface IDrupalSettings {

      images: string[];

    }

  }

  export interface IDrupalSettings {

    simpletest?: SimpleTest.IDrupalSettings;

  }

}
