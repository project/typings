(function (
  $: JQueryStatic,
  drop: drupal.IDrupalStatic,
  drupalSettings: drupal.IDrupalSettings
): void {

  let myBehavior: drupal.Core.IBehavior = {

    attach: function (
      context?: HTMLElement,
      settings?: drupal.IDrupalSettings
    ): void {
      // Empty block.
    },

    detach: function (
      context?: HTMLElement,
      settings?: drupal.IDrupalSettings,
      trigger?: string
    ): void {
      // Empty block.
    }

  };

  let myBool: boolean;
  let myString: string;
  myString = drop.t('foo', {}, {context: 'test'});
  myString += drop.theme('placeholder', myString);

  myString += drop.url(myString);
  myString += drop.url.toAbsolute(myString);
  myBool = drop.url.isLocal(myString);
  myString += drop.stringReplace('foo', {bar: 'baz'}, ['bar']);
  myString += drop.formatString('foo', {bar: 'baz'});
  myString += drop.formatPlural(
    1,
    '@count foo',
    '@count baz',
    {bar: 'baz'},
    {context: 'test'}
  );
  myString += drop.checkPlain('foo');
  drop.attachBehaviors($('#id').get(0), drupalSettings);

  let myPoint: drupal.Core.IPoint = {x: 42, y: 42};
  let myRange: drupal.Core.IRange = {min: myPoint.x, max: myPoint.y};
  let myOffset: drupal.Core.IOffsets = {
    top: 42,
    right: 42,
    bottom: 42,
    left: 42
  };

  drupalSettings.path.baseUrl = myString;
  drupalSettings.path.currentLanguage = myString;
  drupalSettings.path.currentPath = myString;
  drupalSettings.path.currentPathIsAdmin = myString;
  drupalSettings.path.isFront = true;
  drupalSettings.path.pathPrefix = myString;
  drupalSettings.path.scriptPath = myString;
  drupalSettings.user.uid = 42;
  drupalSettings.user.permissionsHash = myString;

  let datePickerOptions: JQueryUI.DatepickerOptions = drupalSettings.jquery.ui.datepicker;

}(jQuery, Drupal, drupalSettings));
