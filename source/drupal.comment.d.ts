
declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      commentFieldsetSummaries?: IBehavior;

      commentByViewer?: IBehavior;

      commentNewIndicator?: IBehavior;

      nodeNewCommentsLink?: IBehavior;

    }

    export namespace Comment {

      export interface IDrupalSettings {

        /**
         * EntityTypeId.FieldName.EntityId: INewLinksEntry.
         */
        newCommentsLinks?: Core.ListStr<Core.ListStr<Core.ListNum<INewLinksEntry>>>;

      }

      export interface INewLinksEntry {

        first_new_comment_link: string;

        new_comment_count: number;

      }

    }

  }

  export interface IDrupalSettings {

    comment?: Core.Comment.IDrupalSettings;

  }

}
