(function (
  $: JQueryStatic,
  drop: drupal.IDrupalStatic,
  drupalSettings: drupal.IDrupalSettings
): void {

  let behavior: drupal.Core.IBehavior = drop.behaviors.tableDrag;
  behavior.attach();

  let str: string;
  let num: number = 0;
  let bool: boolean;

  str = drop.theme('tableDragChangedMarker');
  str += drop.theme('tableDragIndentation');
  str += drop.theme('tableDragChangedWarning');

  let tableId: string = 'a';
  let group: string = 'b';
  let tableOptions: drupal.Core.ITableDragOptions;
  tableOptions = drupalSettings.tableDrag[tableId][group][num];
  tableOptions.action = 'foo';
  tableOptions.hidden = true;
  tableOptions.limit = num;
  tableOptions.relationship = 'parent';
  tableOptions.source = '.selector';
  tableOptions.target = '.selector';

  let joker: any = {};
  let jqEvent: JQueryEventObject = <JQueryEventObject> joker;
  let $jq: JQuery = $('.foo');
  let htmlElement: HTMLElement = $jq.get(0);
  let htmlElements: HTMLElement[] = [];
  let td: drupal.Core.ITableDrag = new drop.tableDrag(htmlElements[0], tableOptions);

  drop.tableDrag[str] = td;

  td.$table = $jq;
  td.table = htmlElement;
  td.tableSettings = tableOptions;
  td.dragObject = htmlElement;
  td.rowObject = htmlElement;
  td.oldRowElement = htmlElement;
  td.oldY = 42;
  td.changed = true;
  td.maxDepth = 42;
  td.rtl = 1;
  td.striping = this;
  td.scrollSettings.amount = 42;
  td.scrollSettings.interval = 42;
  td.scrollSettings.trigger = 42;
  td.scrollInterval = 42;
  td.scrollY = 42;
  td.windowHeight = 42;
  td.indentEnabled = 42;
  td.indentCount = 42;
  td.indentAmount = 42;
  td.initColumns();
  td.addColspanClass(42);
  td.displayColumns(true);
  td.toggleColumns();
  td.hideColumns();
  td.showColumns();
  tableOptions = td.rowSettings('foo', htmlElement);
  td.makeDraggable(htmlElement);
  td.makeDraggable('foo');
  td.dragStart(jqEvent, td, htmlElement);
  td.dragRow(jqEvent, td);
  td.dropRow(jqEvent, td);
  td.pointerCoords(jqEvent);
  td.getPointerOffset(htmlElement, jqEvent);
  htmlElement = td.findDropTargetRow(42, 42);
  td.updateFields(htmlElement);
  td.updateField(htmlElement, 'foo');
  td.copyDragClasses(htmlElement, htmlElement, 'foo');
  num += td.checkScroll(42);
  td.setScroll(num);
  td.restripeTable();
  td.onDrag();
  td.onDrop();

  let tdr: drupal.Core.ITableDragRow = td.row;
  let range: drupal.Core.IRange;
  tdr.element = htmlElement;
  tdr.method = 'foo';
  tdr.group = [htmlElement];
  tdr.groupDepth = 42;
  tdr.changed = true;
  tdr.indentEnabled = true;
  tdr.maxDepth = 42;
  tdr.direction = 'down';
  tdr.indents = 42;
  tdr.children = [htmlElement];
  tdr.interval.min = 42;
  tdr.interval.max = 84;
  htmlElements = tdr.findChildren(true);
  num = tdr.indent(42);
  bool = tdr.isValidSwap(htmlElements[num]);
  if (bool) {
    tdr.swap('up', htmlElement);
  }
  range = tdr.validIndentInterval(htmlElement, htmlElement);
  htmlElements = tdr.findSiblings(tableOptions);
  tdr.removeIndentClasses();
  tdr.markChanged();
  tdr.onIndent();
  tdr.onSwap(htmlElements[range.min]);

}(jQuery, Drupal, drupalSettings));
