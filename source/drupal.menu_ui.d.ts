declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      /**
       * Set a summary on the menu link form.
       */
      menuUiDetailsSummaries?: IBehavior;

      /**
       * Automatically fill in a menu link title, if possible.
       */
      menuUiLinkAutomaticTitle?: IBehavior;

      menuUiChangeParentItems?: IBehavior;

    }

  }

  export interface IDrupalStatic {

    /**
     * Function to set the options of the menu parent item dropdown.
     */
    menuUiUpdateParentList?(): void;

  }

}
