(function (
  drop: drupal.IDrupalStatic,
  drupalSettings: drupal.IDrupalSettings
): void {

  'use strict';

  let behavior: drupal.Core.IBehavior;
  behavior = drop.behaviors.termDrag;
  behavior.attach();

  drupalSettings.taxonomy.backStep = 42;
  drupalSettings.taxonomy.forwardStep = 42;

})(Drupal, drupalSettings);
