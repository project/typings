declare namespace drupal {

  export namespace Core {

    export interface IDrupalSettingsMachineNameEntry {

      target: string;

      suffix: string;

      label: string;

      replace_pattern: string;

      replace: string;

      standalone: boolean;

      field_prefix: string;

      field_suffix: string;

      maxlength: number;

    }

    export interface IEventDataMachineName {

      $source: JQuery;

      $target: JQuery;

      $suffix: JQuery;

      $wrapper: JQuery;

      $preview: JQuery;

      options: IDrupalSettingsMachineNameEntry;

    }

    export interface IBehaviorMachineName extends IBehavior {

      showMachineName(
        machine: string,
        data: IEventDataMachineName
      ): void;

      transliterate(
        source: string,
        settings: IDrupalSettingsMachineNameEntry
      ): string;

    }

    export interface IBehaviors {

      machineName?: IBehaviorMachineName;

    }

  }

  export interface IDrupalSettings {

    machineName?: Core.ListStr<Core.IDrupalSettingsMachineNameEntry>;

  }

}
