declare namespace drupal {

  export namespace Core {

    export interface IAutocompleteItem {

      label?: string;

    }

    export interface IAutocompleteOptions extends JQueryUI.AutocompleteOptions {

      firstCharacterBlacklist?: string;

      renderItem?(ul: HTMLElement, item: IAutocompleteItem): JQuery;

    }

    export interface IAutocomplete {

      /**
       * Key is the element ID.
       *
       * @todo Remove any.
       */
      cache: Core.ListStr<any>;

      splitValues(value: string): string[];

      extractLastTerm(terms: string): string;

      options: IAutocompleteOptions;

      ajax: Core.IJQueryAjaxSettingsExtra;

    }

    export interface IBehaviors {

      autocomplete?: IBehavior;

    }

  }

  export interface IDrupalStatic {

    autocomplete: Core.IAutocomplete;

  }

}
