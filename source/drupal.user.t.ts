(function (
  drop: drupal.IDrupalStatic,
  drupalSettings: drupal.IDrupalSettings
): void {

  'use strict';

  let behavior: drupal.Core.IBehavior;
  behavior = drop.behaviors.password;
  behavior.attach();

  let behaviorToggle: drupal.User.IPermissionsBehavior;
  behaviorToggle = drop.behaviors.permissions;
  behaviorToggle.toggle();

  let msg: drupal.User.IPasswordStrengthTranslations;
  let report: drupal.User.IPasswordStrengthReport;
  msg = {
    showStrengthIndicator: true,
    strengthTitle: 'foo',
    confirmFailure: 'foo',
    confirmSuccess: 'foo',
    confirmTitle: 'foo',
    username: 'foo',
    tooShort: 'foo',
    addLowerCase: 'foo',
    addUpperCase: 'foo',
    addNumbers: 'foo',
    addPunctuation: 'foo',
    sameAsUsername: 'foo',
    weak: 'foo',
    fair: 'foo',
    good: 'foo',
    strong: 'foo',
    hasWeaknesses: 'foo'
  };

  report = drop.evaluatePasswordStrength('foo', msg);
  report.strength = 42;
  report.message = 'foo';
  report.indicatorText = 'foo';
  report.indicatorClass = 'foo';

  drupalSettings.password = msg;

})(Drupal, drupalSettings);
