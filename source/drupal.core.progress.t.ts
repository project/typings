(function (
  drop: drupal.IDrupalStatic
): void {

  let myThemedProgressBar: string = drop.theme('progressBar', 'foo');

  let updateCallback: drupal.Core.IProgressBarUpdateCallback = function (
    percentage: number,
    message: string,
    progressBar: drupal.Core.IProgressBar
  ): void {
    // Empty block.
  };

  let errorCallback: drupal.Core.IProgressBarErrorCallback = function (
    progressBar: drupal.Core.IProgressBar
  ): void {
    // Empty block.
  };

  let myProgressBar: drupal.Core.IProgressBar;
  myProgressBar = new drop.ProgressBar(
    'foo',
    updateCallback,
    'POST',
    errorCallback
  );

  myProgressBar.setProgress(42, 'Foo', 'foo');
  myProgressBar.startMonitoring('Foo', 42);
  myProgressBar.stopMonitoring();
  myProgressBar.sendPing();
  myProgressBar.displayError('Foo');

}(Drupal));
