declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      /**
       * Initialize tableSelects.
       */
      tableSelect?: IBehavior;

    }

    export interface ITableSelect {

      (): void;

    }

    export interface ITableSelectRange {

      /**
       * @param from
       *   The HTML element representing the "from" part of the range.
       * @param to
       *   The HTML element representing the "to" part of the range.
       * @param state
       *   The state to set on the range.
       */
      (
        from: HTMLElement,
        to: HTMLElement,
        state: boolean
      ): void;

    }

  }

  export interface IDrupalStatic {

    /**
     * Callback used in {@link Drupal.behaviors.tableSelect}.
     */
    tableSelect?: Core.ITableSelect;

    tableSelectRange?: Core.ITableSelectRange;

  }

}
