declare namespace drupal {

  export namespace Core {

    export interface IDrupalSettingsDialog extends JQueryUI.DialogOptions {

      buttonClass?: string;

      buttonPrimaryClass?: string;

      autoResize?: boolean;

      drupalAutoButtons?: boolean;

    }

    export interface IDialogButtonDefinition {

      text: string;

      'class': string;

      click(e: JQueryEventObject): void;

    }

    export interface IBehaviorDialog extends IBehavior {

      prepareDialogButtons($dialog: JQuery): JQueryUI.DialogButtonOptions[];

    }

    export interface IBehaviors {

      dialog?: IBehaviorDialog;

    }

    export interface IDialogHandler {

      open: boolean;

      returnValue: any;

      show(): void;

      showModal(): void;

      close(value: any): void;

    }

    export interface IAjaxCommands {

      openDialog?: IAjaxCommand;

      closeDialog?: IAjaxCommand;

      setDialogOption?: IAjaxCommand;

    }

  }

  export interface IDrupalSettings {

    dialog?: Core.IDrupalSettingsDialog;

  }

  export interface IDrupalStatic {

    dialog?(
      element: HTMLElement,
      options: Core.IDrupalSettingsDialog
    ): Core.IDialogHandler;

  }

}
