declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      /**
       * Enables editors on text_format elements.
       */
      editor: IBehavior;

      /**
       * Initializes {@link Drupal.filterConfiguration}.
       */
      initializeFilterConfiguration: IBehavior;

    }

    export interface IAjaxCommands {

      /**
       * Command to save the contents of an editor-provided modal.
       *
       * This command does not close the open modal. It should be followed
       * by a call to `Drupal.AjaxCommands.prototype.closeDialog`.
       * Editors that are integrated with dialogs must independently
       * listen for an `editor:dialogsave` event to save the changes into
       * the contents of their interface.
       *
       * @fires event:editor:dialogsave
       */
      editorDialogSave?: IAjaxCommand;

    }

  }

  export namespace Editor {

    export interface IDrupalSettings {

      formats: Core.ListStr<IEditorSettingsBase>;

    }

    export interface IEditor {
    }

    export interface IEditorConfiguration {

      /**
       * Must be called by a specific text editor's configuration whenever a
       * feature is added by the user.
       *
       * Triggers the drupalEditorFeatureAdded event on the document, which
       * receives a {@link Drupal.EditorFeature} object.
       *
       * @param feature
       *   A text editor feature object.
       */
      addedFeature?(feature: IEditorFeature): void;

      /**
       * Must be called by a specific text editor's configuration whenever a
       * feature is removed by the user.
       *
       * Triggers the drupalEditorFeatureRemoved event on the document, which
       * receives a {@link Drupal.EditorFeature} object.
       *
       * @param feature
       *   A text editor feature object.
       */
      removedFeature(feature: IEditorFeature): void;

      /**
       * Must be called by a specific text editor's configuration whenever a
       * feature is modified, i.e. has different rules.
       *
       * For example when the "Bold" button is configured to use the `<b>` tag
       * instead of the `<strong>` tag.
       *
       * Triggers the drupalEditorFeatureModified event on the document, which
       * receives a {@link Drupal.EditorFeature} object.
       *
       * @param feature
       *   A text editor feature object.
       */
      modifiedFeature(feature: IEditorFeature): void;

      /**
       * May be called by a specific text editor's configuration whenever a
       * feature is being added, to check whether it would require the filter
       * settings to be updated.
       *
       * The canonical use case is when a text editor is being enabled:
       * preferably
       * this would not cause the filter settings to be changed; rather, the
       * default set of buttons (features) for the text editor should adjust
       * itself to not cause filter setting changes.
       *
       * Note: for filters to integrate with this functionality, it is necessary
       * that they implement
       * `Drupal.filterSettingsForEditors[filterID].getRules()`.
       *
       * @param feature
       *   A text editor feature object.
       *
       * @return
       *   Whether the given feature is allowed by the current filters.
       */
      featureIsAllowedByFilters(feature: IEditorFeature): boolean;

    }

    export interface IEditorFeatureStatic {

      /**
       * @param name
       *   The name of the feature.
       */
      new (name: string): IEditorFeature;

    }

    export interface IEditorFeature {

      name: string;

      rules: IEditorFeatureHTMLRule[];

      /**
       * Adds a HTML rule to the list of HTML rules for this feature.
       *
       * @param rule
       *   A text editor feature HTML rule.
       */
      addHTMLRule(rule: IEditorFeatureHTMLRule): void;

    }

    export interface IEditorFeatureHTMLRuleStatic {

      new (): IEditorFeatureHTMLRule;

    }

    export interface IEditorFeatureHTMLRule {

      required: IEditorFeatureHTMLRuleProperties;

      allowed: IEditorFeatureHTMLRuleProperties;

      raw: any;

    }

    export interface IEditorFeatureHTMLRuleProperties extends IFilterHTMLRuleProperties {

      tags: string[];

    }

    export interface IFormat {

      editor: string;

      editorSettings: IEditorSettingsBase;

      editorSupportsContentFiltering: boolean;

      format: string;

      isXssSafe: boolean;

    }

    export interface IEditorSettingsBase {
    }

    export interface IFilterStatusStatic {

      /**
       * Text filter status object. Initialized with the filter ID.
       *
       * Indicates whether the text filter is currently active (enabled) or not.
       *
       * Contains a set of HTML rules ({@link Drupal.FilterHTMLRule} objects) that
       * describe which HTML tags are allowed or forbidden. They can also describe
       * for a set of tags (or all tags) which attributes, styles and classes are
       * allowed and which are forbidden.
       *
       * It is necessary to allow for multiple HTML rules per feature, for
       * analogous reasons as {@link Drupal.EditorFeature}.
       *
       * HTML rules must be added with the `addHTMLRule()` method. A filter that has
       * zero HTML rules does not disallow any HTML.
       *
       * @constructor
       *
       * @param name
       *   The name of the feature.
       *
       * @see Drupal.FilterHTMLRule
       */
      new (name: string): IFilterStatus;

    }

    export interface IFilterStatus {

      name: string;

      active: boolean;

      rules: IFilterHTMLRule[];

      /**
       * Adds a HTML rule to the list of HTML rules for this filter.
       *
       * @param rule
       *   A text filter HTML rule.
       */
      addHTMLRule(rule: IFilterHTMLRule): void;

    }

    export interface IFilterHTMLRuleStatic {

      new (): IFilterHTMLRule;

    }

    export interface IFilterHTMLRule {

      tags: string[];

      allow: string[];

      restrictedTags: {

        tags: string[];

        allowed: IEditorFeatureHTMLRuleProperties;

        forbidden: IEditorFeatureHTMLRuleProperties;

      };

      clone(): this;

    }

    export interface IFilterHTMLRuleProperties {

      attributes: string[];

      styles: string[];

      classes: string[];

    }

    export interface IFilterConfiguration {

      /**
       * Drupal.FilterStatus objects, keyed by filter ID.
       */
      statuses: drupal.Core.ListStr<IFilterStatus>;

      /**
       * Live filter setting parsers.
       *
       * Object keyed by filter ID, for those filters that implement it.
       *
       * Filters should load the implementing JavaScript on the filter
       * configuration form and implement
       * `Drupal.filterSettings[filterID].getRules()`, which should return an
       * array of {@link Drupal.FilterHTMLRule} objects.
       *
       * @namespace
       */
      liveSettingParsers: {

        // @todo Remove any.
        // @todo Use collection.
        [filterId: string]: any;

      };

      /**
       * Updates all {@link Drupal.FilterStatus} objects to reflect current state.
       *
       * Automatically checks whether a filter is currently enabled or
       * not. To support more fine-grained.
       *
       * If a filter implements a live setting parser, then that will be
       * used to keep the HTML rules for the {@link Drupal.FilterStatus}
       * object up-to-date.
       */
      update(): void;

    }

  }

  export interface IDrupalSettings {

    editor?: Editor.IDrupalSettings;

  }

  export interface IDrupalStatic {

    editors?: {

      // @todo Use collection.
      [editor: string]: Editor.IEditor;

    };

    /**
     * Attaches editor behaviors to the field.
     *
     * @param field
     *   The textarea DOM element.
     * @param format
     *   The text format that's being activated, from
     *   drupalSettings.editor.formats.
     */
    editorAttach?(
      field: HTMLTextAreaElement,
      format: Editor.IFormat
    ): void;

    /**
     * Detaches editor behaviors from the field.
     *
     * @param field
     *   The textarea DOM element.
     * @param format
     *   The text format that's being activated, from
     *   drupalSettings.editor.formats.
     * @param trigger
     *   Trigger value from the detach behavior.
     */
    editorDetach?(
      field: HTMLTextAreaElement,
      format: Editor.IFormat,
      trigger: string
    ): void;

    /**
     * Editor configuration namespace.
     */
    editorConfiguration?: Editor.IEditorConfiguration;

    /**
     * Constructor for an editor feature HTML rule.
     *
     * Intended to be used in combination with {@link Drupal.EditorFeature}.
     *
     * A text editor feature rule object describes both:
     *  - required HTML tags, attributes, styles and classes: without these, the
     *    text editor feature is unable to function. It's possible that a
     *  - allowed HTML tags, attributes, styles and classes: these are optional
     *    in the strictest sense, but it is possible that the feature generates
     *    them.
     *
     * The structure can be very clearly seen below: there's a "required" and an
     * "allowed" key. For each of those, there are objects with the "tags",
     * "attributes", "styles" and "classes" keys. For all these keys the values
     * are initialized to the empty array. List each possible value as an array
     * value. Besides the "required" and "allowed" keys, there's an optional
     * "raw" key: it allows text editor implementations to optionally pass in
     * their raw representation instead of the Drupal-defined representation for
     * HTML rules.
     *
     * @example
     * tags: ['<a>']
     * attributes: ['href', 'alt']
     * styles: ['color', 'text-decoration']
     * classes: ['external', 'internal']
     *
     * @constructor
     *
     * @see Drupal.EditorFeature
     */
    EditorFeatureHTMLRule?: Editor.IEditorFeatureHTMLRuleStatic;

    /**
     * A text editor feature object. Initialized with the feature name.
     *
     * Contains a set of HTML rules ({@link Drupal.EditorFeatureHTMLRule} objects)
     * that describe which HTML tags, attributes, styles and classes are required
     * (i.e. essential for the feature to function at all) and which are allowed
     * (i.e. the feature may generate this, but they're not essential).
     *
     * It is necessary to allow for multiple HTML rules per feature: with just
     * one HTML rule per feature, there is not enough expressiveness to describe
     * certain cases. For example: a "table" feature would probably require the
     * `<table>` tag, and might allow e.g. the "summary" attribute on that tag.
     * However, the table feature would also require the `<tr>` and `<td>` tags,
     * but it doesn't make sense to allow for a "summary" attribute on these tags.
     * Hence these would need to be split in two separate rules.
     *
     * HTML rules must be added with the `addHTMLRule()` method. A feature that
     * has zero HTML rules does not create or modify HTML.
     *
     * @see Drupal.EditorFeatureHTMLRule
     */
    EditorFeature?: Editor.IEditorFeatureStatic;

    FilterStatus?: Editor.IFilterStatusStatic;

    FilterHTMLRule?: Editor.IFilterHTMLRuleStatic;

    filterConfiguration?: Editor.IFilterConfiguration;

    // @todo File core/modules/editor/js/editor.formattedTextEditor.js.

  }

}
