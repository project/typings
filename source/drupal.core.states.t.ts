(function (
  drop: drupal.IDrupalStatic
): void {

  let myState: drupal.Core.IState = new drop.states.State('foo');
  myState.invert = false;
  myState.name = 'foo';
  myState.pristine = 'foo';

  drop.states.State.aliases['not okay'] = '!okay';

}(Drupal));
