declare namespace drupal {

  export namespace Core {

    export interface IBehaviors {

      ViewsAjaxView?: IBehavior;

    }

  }

  export namespace Views {

    export interface IViewArgs {

      view_args?: string;

      view_path?: string;

    }

    export interface ITools {

      /**
       * Helper function to parse a QueryString.
       *
       * @param query
       *   The QueryString to parse.
       *
       * @return
       *   A map of query parameters.
       */
      parseQueryString(query: string): {[key: string]: string};

      /**
       * Helper function to return a view's arguments based on a path.
       *
       * @param href
       *   The href to check.
       * @param viewPath
       *   The views path to check.
       *
       * @return {object}
       *   An object containing `view_args` and `view_path`.
       */
      parseViewArgs(href: string, viewPath: string): IViewArgs;

      /**
       * Strip off the protocol plus domain from an href.
       *
       * @param href
       *   The href to strip.
       *
       * @return
       *   The href without the protocol and domain.
       */
      pathPortionfunction(href: string): string;

      /**
       * Return the Drupal path portion of an href.
       *
       * @param href
       *   The href to check.
       *
       * @return
       *   An internal path.
       */
      getPath(href: string): string;

    }

    export interface IAjaxViewSettings {

      /**
       * The DOM id of the view
       */
      view_dom_id: string;

      view_base_path: string;

      view_name: string;

      view_display_id: string;

    }

    export interface IViews {

      instances: IAjaxView[];

      ajaxView: IAjaxViewStatic;

    }

    export interface IAjaxViewStatic {

      instances: IAjaxView[];

      /**
       * Javascript object for a certain view.
       *
       * @param settings
       *   Settings object for the ajax view.
       */
      new (settings: IAjaxViewSettings): IAjaxView;

    }

    export interface IAjaxView {

      // @todo Remove any.
      exposedFormAjax: any;

      $view: JQuery;

      // @todo Remove any.
      element_settings: any;

      settings: IAjaxViewSettings;

      $exposed_form: JQuery;

      // @todo Remove any.
      refreshViewAjax: any;

      attachExposedFormAjax(): void;

      /**
       * @return
       *    If there is at least one parent with a view class return false.
       */
      filterNestedViews(): boolean;

      /**
       * Attach the ajax behavior to each link.
       */
      attachPagerAjax(): void;

      /**
       * Attach the ajax behavior to a singe link.
       *
       * @param [id]
       *   The ID of the link.
       * @param [link]
       *   The link element.
       */
      attachPagerLinkAjax(id?: string, link?: HTMLElement): void;

      /**
       * Views scroll to top ajax command.
       *
       * @param ajax
       * @param response
       */
      viewsScrollTop(
        ajax?: drupal.Core.Iajax,
        // @todo Remove any.
        response?: any
      ): void;

    }

    export interface IDrupalSettings {

      /**
       * Key is the view->name.
       */
      ajaxViews?: drupal.Core.ListStr<IAjaxViewSettings>;

    }

  }

  export interface IDrupalSettings {

    views?: drupal.Views.IDrupalSettings;

  }

  export interface IDrupalStatic {

    Views?: Views.ITools;

    views?: Views.IViews;

  }

}
