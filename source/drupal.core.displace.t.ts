(function (
  drop: drupal.IDrupalStatic
): void {

  let behavior: drupal.Core.IBehavior;
  behavior = drop.behaviors.drupalDisplace;

  let offset: drupal.Core.IOffsets = drop.displace(true);
  offset.top = 42;
  offset.left = 42;
  offset.bottom = 42;
  offset.right = 42;

  let myDisplace: drupal.Core.IDisplace = new drop.displace();
  myDisplace.offsets.top = myDisplace.calculateOffset('top');

}(Drupal));
