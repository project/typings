(function (
  $: JQueryStatic,
  drop: drupal.IDrupalStatic
): void {

  let collapsibleDetails: drupal.Core.ICollapsibleDetails;
  collapsibleDetails = new drop.CollapsibleDetails($('details').get(0));

  let callbackNoneVoid: {(): void} = function (): void {
    // Empty block.
  };
  let callbackEventVoid: {(e: JQueryEventObject): void} = function (e: JQueryEventObject): void {
    // Empty block.
  };

  drop.CollapsibleDetails.instances.push(collapsibleDetails);
  collapsibleDetails.setupSummary = callbackNoneVoid;
  collapsibleDetails.setupLegend = callbackNoneVoid;
  collapsibleDetails.onLegendClick = callbackEventVoid;
  collapsibleDetails.onSummaryUpdated = callbackNoneVoid;
  collapsibleDetails.toggle = callbackNoneVoid;

}(jQuery, Drupal));
