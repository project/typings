(function (
  drop: drupal.IDrupalStatic
): void {

  'use strict';

  let behavior: drupal.Core.IBehavior;
  behavior = drop.behaviors.trackerHistory;
  behavior.attach();

})(Drupal);
