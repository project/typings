(function (
  $: JQueryStatic
): void {

  let summaryGetter: {(element: HTMLElement): string} = function (element: HTMLElement): string {
    return 'My summary 02';
  };

  let summary01: string = 'My summary 01';

  let summary02: string = $('input.foo-1')
    .drupalSetSummary(summary01)
    .drupalGetSummary();

  $('input.foo-2').drupalSetSummary(summaryGetter);

}(jQuery));
