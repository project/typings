(function (
  $: JQueryStatic,
  drop: drupal.IDrupalStatic
): void {

  let behavior: drupal.Core.IBehavior = drop.behaviors.tableResponsive;

  let table: HTMLElement = $('table').get(0);
  drop.TableResponsive.tables.push(new drop.TableResponsive(table));

  let $jq: JQuery = $('foo');
  let tr: drupal.Core.ITableResponsive = drop.TableResponsive.tables[0];

  tr.table = $jq.get(0);
  tr.$table = $jq;
  tr.$headers = $jq;
  tr.showText = 'foo';
  tr.$link = $jq;
  tr.$revealedCells = $jq;

  tr.eventhandlerEvaluateColumnVisibility = function (event: JQueryEventObject): void {
    tr.showText = event.namespace;
  };

  tr.eventhandlerToggleColumns = tr.eventhandlerEvaluateColumnVisibility;

})(jQuery, Drupal);
