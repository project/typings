(function (
  drop: drupal.IDrupalStatic,
  drupalSettings: drupal.IDrupalSettings
): void {

  let behavior: drupal.Core.IBehavior;
  behavior = drop.behaviors.editor;
  behavior = drop.behaviors.initializeFilterConfiguration;

  let ajaxCommand: drupal.Core.IAjaxCommand;
  ajaxCommand = drop.AjaxCommands.editorDialogSave;

  let str: string;
  str = 'foo';

  let htmlTextAreaElement: HTMLTextAreaElement;

  // @todo Better test.
  drupalSettings.editor.formats[str] = {};

  // @todo Better test.
  drop.editors[str] = {};

  let editorFormat: drupal.Editor.IFormat;
  editorFormat = {
    editor: 'ckeditor',
    editorSettings: {},
    editorSupportsContentFiltering: true,
    format: 'filtered_html',
    isXssSafe: false
  };

  drop.editorAttach(htmlTextAreaElement, editorFormat);
  drop.editorDetach(htmlTextAreaElement, editorFormat, str);

  let editorFeatureHtmlRule: drupal.Editor.IEditorFeatureHTMLRule;
  editorFeatureHtmlRule = new drop.EditorFeatureHTMLRule();

  let editorFeatureHTMLRuleProperties: drupal.Editor.IEditorFeatureHTMLRuleProperties;
  editorFeatureHTMLRuleProperties.tags.push(str);
  editorFeatureHTMLRuleProperties.attributes.push(str);
  editorFeatureHTMLRuleProperties.styles.push(str);
  editorFeatureHTMLRuleProperties.classes.push(str);

  editorFeatureHTMLRuleProperties = editorFeatureHtmlRule.allowed;
  editorFeatureHTMLRuleProperties = editorFeatureHtmlRule.required;

  let editorFeature: drupal.Editor.IEditorFeature;
  editorFeature = new drop.EditorFeature(str);
  editorFeature.addHTMLRule(editorFeatureHtmlRule);
  editorFeature.name = str;
  editorFeature.rules.push(editorFeatureHtmlRule);

  let filterHTMLRule: drupal.Editor.IFilterHTMLRule;
  filterHTMLRule = new drop.FilterHTMLRule();
  filterHTMLRule.tags.push(str);
  filterHTMLRule.allow.push(str);
  filterHTMLRule.restrictedTags.tags.push(str);
  filterHTMLRule.restrictedTags.allowed = editorFeatureHTMLRuleProperties;
  filterHTMLRule.restrictedTags.forbidden = editorFeatureHTMLRuleProperties;
  filterHTMLRule = filterHTMLRule.clone();

  let filterStatus: drupal.Editor.IFilterStatus;
  filterStatus = new drop.FilterStatus(str);
  filterStatus.active = true;
  filterStatus.name = str;
  filterStatus.rules.push(filterHTMLRule);
  filterStatus.addHTMLRule(filterHTMLRule);

  drop.filterConfiguration.statuses[str] = filterStatus;
  drop.filterConfiguration.update();

})(Drupal, drupalSettings);
