declare namespace drupal {

  export namespace Core {

    export interface ICollapsibleDetailsStatic {

      instances?: ICollapsibleDetails[];

      new (node: HTMLElement): ICollapsibleDetails;

    }

    export interface ICollapsibleDetails {

      $node: JQuery;

      $summary: JQuery;

      setupSummary?(): void;

      setupLegend?(): void;

      onLegendClick?(e: JQueryEventObject): void;

      onSummaryUpdated?(): void;

      toggle?(): void;

    }

    export interface IBehaviors {

      collapse?: IBehavior;

    }

  }

  export interface IDrupalStatic {

    CollapsibleDetails?: Core.ICollapsibleDetailsStatic;

  }
}
