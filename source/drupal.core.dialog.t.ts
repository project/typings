(function (
  $: JQueryStatic,
  drop: drupal.IDrupalStatic,
  drupalSettings: drupal.IDrupalSettings
): void {

  let behavior: drupal.Core.IBehaviorDialog;

  behavior = drop.behaviors.dialog;

  let dialogButtonOptions: JQueryUI.DialogButtonOptions[];
  dialogButtonOptions = behavior.prepareDialogButtons($('#foo'));
  dialogButtonOptions[0].text = 'foo';
  dialogButtonOptions[0].showText = true;
  $('#bar').click(dialogButtonOptions[0].click);

  let dialogHandler: drupal.Core.IDialogHandler;
  let element: HTMLElement = document.getElementById('foo');
  let dialogOptions: drupal.Core.IDrupalSettingsDialog;

  dialogOptions = {
    autoResize: true,
    maxHeight: 100
  };
  dialogOptions.buttonClass = 'foo';
  dialogOptions.buttonPrimaryClass = 'foo';

  dialogHandler = drop.dialog(element, dialogOptions);
  dialogHandler.open = true;
  dialogHandler.returnValue = true;
  dialogHandler.returnValue = 42;
  dialogHandler.show();
  dialogHandler.showModal();
  dialogHandler.close(false);
  dialogHandler.close(42);

  let ajaxCommand: drupal.Core.IAjaxCommand;

  ajaxCommand = drop.AjaxCommands.openDialog;
  ajaxCommand = drop.AjaxCommands.closeDialog;
  ajaxCommand = drop.AjaxCommands.setDialogOption;

  dialogOptions = drupalSettings.dialog;

}(jQuery, Drupal, drupalSettings));
