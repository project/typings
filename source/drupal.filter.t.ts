(function (
  drop: drupal.IDrupalStatic
): void {

  let behavior: drupal.Core.IBehavior;

  drop.behaviors.filterGuidelines = behavior;
  drop.behaviors.filterStatus = behavior;

})(Drupal);
